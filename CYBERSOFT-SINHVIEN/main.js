function getNameFromTr(trTag) {
  var list = trTag.querySelectorAll('td');
  console.log(`  🚀: getNameFromTr -> list`, list);
  return list[2].innerText;
}
function getScoreFromTr(trTag) {
  var list = trTag.querySelectorAll('td');
  return list[3].innerText * 1;
}
// tất cả thẻ .td-score đang có trên layout
var danhSachTd = document.querySelectorAll('.td-scores');

var danhSachTr = document.querySelectorAll('#tblBody tr');

var scoreArr = [];

for (var i = 0; i < danhSachTd.length; i++) {
  var score = danhSachTd[i].innerText * 1;
  scoreArr.push(score);
}

var scoreMax = scoreArr[0];
var indexMax = 0;
console.log(`  🚀: scoreArr`, scoreArr);
for (var i = 1; i < scoreArr.length; i++) {
  if (scoreArr[i] > scoreMax) {
    // nếu giá hiện tại lớn hơn giá trị lớn nhất thì update
    scoreMax = scoreArr[i];
    indexMax = i;
  }
}

// show sinh viên giỏi nhất
document.getElementById('svGioiNhat').innerHTML = `
<h3>
${getNameFromTr(danhSachTr[indexMax])}
- 
${getScoreFromTr(danhSachTr[indexMax])}
</h3>
`;
// số lượng sinh viên lớn hơn 5

var soSinhVienGioi = 0;

for (var i = 0; i < danhSachTr.length; i++) {
  var score = getScoreFromTr(danhSachTr[i]);
  if (score >= 8) {
    soSinhVienGioi++;
  }
}
console.log(`  🚀: soSinhVienGioi`, soSinhVienGioi);
document.getElementById('soSVGioi').innerText = soSinhVienGioi;
//  danh sách sinh viên lớn hơn 5

var stringDanhSach = '';
for (var i = 0; i < danhSachTr.length; i++) {
  var score = getScoreFromTr(danhSachTr[i]);
  if (score >= 5) {
    var content = `<p class="text-success">
        ${getNameFromTr(danhSachTr[i])} - ${getScoreFromTr(danhSachTr[i])}
      </p>`;
    stringDanhSach += content;
  }
}
document.getElementById('dsDiemHon5').innerHTML = stringDanhSach;

// bubble sort
// https://vercel.com/
